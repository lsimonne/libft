/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsimonne <lsimonne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 15:48:53 by lsimonne          #+#    #+#             */
/*   Updated: 2015/12/04 12:44:42 by lsimonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcat(char *dst, const char *src, size_t size)
{
	char		*dest;
	const char	*srce;
	size_t		i;
	size_t		len;

	dest = dst;
	srce = src;
	len = size;
	while (len-- != 0 && *dest != '\0')
		dest++;
	i = dest - dst;
	if ((len = size - i) <= 0)
		return (size + ft_strlen(srce));
	while (*srce && len != 1)
	{
		*dest = *srce;
		len--;
		dest++;
		srce++;
	}
	if (*(srce + 1))
		*dest = '\0';
	else
		*dest = *++srce;
	return (i + srce - src);
}
