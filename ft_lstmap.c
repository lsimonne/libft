/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsimonne <lsimonne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/03 15:10:58 by lsimonne          #+#    #+#             */
/*   Updated: 2015/12/03 15:28:45 by lsimonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

t_list	*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list	*flst;
	t_list	*lststart;

	lststart = NULL;
	flst = NULL;
	if (lst)
	{
		flst = f(ft_lstnew(lst->content, lst->content_size));
		lststart = flst;
		lst = lst->next;
	}
	while (lst)
	{
		flst->next = f(ft_lstnew(lst->content, lst->content_size));
		flst = flst->next;
		lst = lst->next;
	}
	return (lststart);
}
