/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_duplicate_array.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsimonne <lsimonne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/10 15:15:32 by lsimonne          #+#    #+#             */
/*   Updated: 2016/03/10 15:17:56 by lsimonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	**ft_duplicate_array(char **array)
{
	char	**dest;
	int		i;

	i = 0;
	while (array[i])
		i++;
	if (!(dest = (char **)malloc(sizeof(char *) * (i + 1))))
		return (NULL);
	dest[i] = NULL;
	i = 0;
	while (array[i])
	{
		dest[i] = ft_strdup(array[i]);
		i++;
	}
	return (dest);
}
