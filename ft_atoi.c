/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsimonne <lsimonne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 15:16:29 by lsimonne          #+#    #+#             */
/*   Updated: 2015/12/04 14:05:13 by lsimonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_atoi(const char *str)
{
	int value;
	int i;
	int j;

	i = 0;
	value = 0;
	j = 0;
	while (str[i] == ' ' || str[i] == '\n' || str[i] == '\t' || str[i] == '\r' \
			|| str[i] == '\v' || str[i] == '\f')
		i++;
	j = i;
	if (str[i] == '-' || str[i] == '+')
		i++;
	while (str[i] != '\0' && str[i] >= 48 && str[i] <= 57)
	{
		value = value * 10 + (str[i] - 48);
		i++;
	}
	if (str[j] == 45)
		value = -value;
	return (value);
}
