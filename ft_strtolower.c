#include "libft.h"

void	ft_str_tolower(char **str)
{
	size_t	i;

	i = 0;
	while (*str && (*str)[i])
	{
		(*str)[i] = ft_tolower((*str)[i]);
		i++;
	}
}
