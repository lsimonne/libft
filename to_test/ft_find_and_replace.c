/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_find_and_replace.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsimonne <lsimonne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/22 10:09:47 by lsimonne          #+#    #+#             */
/*   Updated: 2016/03/22 11:00:16 by lsimonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <sys/types.h>

char	*ft_find_and_replace(char *str, char *find, char *replace)
{
	size_t	i;
	size_t	j;
	char	*dest;
	char	*tmp;
	char	*tmp2;

	i = 0;
	if (ft_strlen(find) == 1)
		return (ft_replace_char(str, *find, replace));
	while (str[i])
	{
		j = 0;
		while (find[j] && str[i] && find[j] == str[i++])
			j++;
		if (!find[j])
		{
			tmp = ft_strsub(str, 0, i - j - ft_strlen(find) - 1);
			ft_putendl(tmp);
			tmp2 = ft_strjoin(tmp, replace);
			ft_strdel(&tmp);
			tmp = ft_strsub(str, i - j - 1, ft_strlen(str) - j - \
					ft_strlen(find));
			dest = ft_strjoin(tmp2, tmp);
			ft_strdel(&tmp);
			ft_strdel(&tmp2);
			return (dest);
		}
		i++;
	}
	return (NULL);
}
