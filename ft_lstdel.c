/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsimonne <lsimonne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/03 15:09:22 by lsimonne          #+#    #+#             */
/*   Updated: 2015/12/03 15:12:38 by lsimonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

void	ft_lstdel(t_list **alst, void (*del)(void *, size_t))
{
	t_list	*lst;
	t_list	*flst;

	lst = *alst;
	while (lst)
	{
		del(lst->content, lst->content_size);
		flst = lst;
		lst = lst->next;
		free(flst);
	}
	*alst = NULL;
}
