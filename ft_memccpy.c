/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsimonne <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/01 18:37:20 by lsimonne          #+#    #+#             */
/*   Updated: 2015/12/02 15:54:29 by lsimonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memccpy(void *dst, const void *src, int c, size_t n)
{
	unsigned char	*dest;
	unsigned char	*s;

	dest = (unsigned char *)dst;
	s = (unsigned char *)src;
	while (n > 0)
	{
		if (*s == (unsigned char)c)
		{
			*dest = *s;
			dest++;
			return ((void *)dest);
		}
		*dest = *s;
		dest++;
		s++;
		n--;
	}
	return (NULL);
}
