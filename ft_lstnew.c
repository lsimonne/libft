/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsimonne <lsimonne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/03 15:10:33 by lsimonne          #+#    #+#             */
/*   Updated: 2015/12/03 15:13:16 by lsimonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

t_list	*ft_lstnew(void const *content, size_t content_size)
{
	t_list	*newlink;

	if (!(newlink = (t_list *)ft_memalloc(sizeof(t_list))))
		return (NULL);
	if (content)
	{
		newlink->content = malloc(content_size);
		newlink->content = ft_memcpy((newlink->content), content, content_size);
		newlink->content_size = content_size;
	}
	else
	{
		newlink->content = NULL;
		newlink->content = 0;
	}
	newlink->next = NULL;
	return (newlink);
}
