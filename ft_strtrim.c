/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsimonne <lsimonne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 15:04:49 by lsimonne          #+#    #+#             */
/*   Updated: 2015/12/08 15:31:19 by lsimonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

char	*ft_strtrim(char const *s)
{
	char	*str;
	int		i;
	size_t	j;
	int		len;

	str = NULL;
	if (s)
	{
		i = 0;
		j = ft_strlen(s);
		while (s[i] && (s[i] == ' ' || s[i] == '\n' || s[i] == '\t'))
			i++;
		if (s[i])
		{
			while (s[j - 1] && (s[j -  1] == ' ' || s[j - 1] == '\n' || s[j - 1] == '\t'))
				j--;
			len = j - i;
			if (!(str = (char *)malloc(sizeof(char) * (len + 1))))
				return (NULL);
			j = 0;
			str[len] = '\0';
			while (--len >= 0)
				str[j++] = s[i++];
		}
		else
			str = ft_strnew(0);
	}
	return (str);
}
