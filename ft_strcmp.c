/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsimonne <lsimonne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 15:40:13 by lsimonne          #+#    #+#             */
/*   Updated: 2015/11/25 15:41:34 by lsimonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_strcmp(const char *s1, const char *s2)
{
	int i;
	int c;

	i = 0;
	while (s1[i] == s2[i] && s2[i] && s1[i])
		i++;
	c = (unsigned char)s1[i] - (unsigned char)s2[i];
	return (c);
}
