/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ishexa.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsimonne <lsimonne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/29 13:21:54 by lsimonne          #+#    #+#             */
/*   Updated: 2016/08/29 13:21:57 by lsimonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_ishexa(char *s)
{
	int		len;

	len = 0;
	if (s)
	{
		if (s[0] == '0' && (*(s + 1) == 'x' || *(s + 1) == 'X'))
		{
			len += 2;
			s += 2;
			while (*s && (ft_isdigit(*s) || ft_strchr("abcdefABCDEF", (int)*s)))
			{
				len++;
				s++;
			}
		}
	}
	return (len);
}
