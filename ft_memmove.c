/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsimonne <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/01 18:36:39 by lsimonne          #+#    #+#             */
/*   Updated: 2015/12/01 18:36:41 by lsimonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

void	*ft_memmove(void *dst, const void *src, size_t len)
{
	char	*str;

	str = (char *)malloc(sizeof(char) * len);
	if (str)
	{
		ft_memcpy(str, src, len);
		ft_memcpy(dst, str, len);
		free(str);
	}
	return (dst);
}
