#include "libft.h"

void	*memalloc_or_die(size_t size)
{
	void	*m;

	if (!(m = ft_memalloc(size)))
	{
		ft_putendl("Malloc error");
		exit(-1);
	}
	return(m);
}
