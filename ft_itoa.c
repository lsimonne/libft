/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lsimonne <lsimonne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/02 15:58:25 by lsimonne          #+#    #+#             */
/*   Updated: 2015/12/03 15:13:38 by lsimonne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

static int		ft_abs(int n)
{
	if (n < 0)
		n = -n;
	return (n);
}

static int		ft_length(int n)
{
	int len;

	len = 0;
	if (n == 0)
		return (1);
	while (n != 0)
	{
		len++;
		n = n / 10;
	}
	return (len);
}

static int		ft_isneg(int n)
{
	if (n < 0)
		return (1);
	else
		return (0);
}

char			*ft_itoa(int n)
{
	char	*nbr;
	int		len;
	int		neg;

	if (n == -2147483648)
		return (ft_strdup("-2147483648"));
	neg = ft_isneg(n);
	len = ft_length(n);
	if (!(nbr = (char *)malloc(sizeof(char) * (len + neg + 1))))
		return (NULL);
	nbr = nbr + len + neg;
	*nbr = '\0';
	nbr--;
	n = ft_abs(n);
	while (n >= 10)
	{
		*nbr = (n % 10) + 48;
		n = n / 10;
		nbr--;
	}
	*nbr = n + 48;
	if (neg)
		*--nbr = '-';
	return (nbr);
}
